#include "RNA.h"
#include <math.h>
#include <string.h>

//Estrutura da Rede
int noOfInputs[NO_OF_LAYERS] = {6,7}; //numero de entradas em cada camada; 0 para a escondida e 1 para de sa�da;
int layers[NO_OF_LAYERS] = {7,3}; //numero de neuronios em cada camada; 0 para a escondida e 1 para de sa�da;
char axonFamilies[] = {'t','t'}; //fun��es de ativa��o de cada camada; 0 para a escondida e 1 para de sa�da;

double neuronInput(__psv__ double *weights,int noOfWeights,int index, double *input){// funcao que calcula o a ativacao(somatorio) do neuronio;
    /* VARIAVEIS
        INT
        i= inteiro de controle;

        DOUBLE
        summatory = vari�vel que armazena o somat�rio (ativa��o)
    */

	int i;

	double summatory = 0;
    
    double Var1, Var2;

	for (i=0;i<noOfWeights;i++){                            //percorre o vetor de pesos i vezes (o n�mero de pesos da camada)
        if (i==0){
			//imprime(BIAS);
			//imprime(*(weights+i+index));                                          //caso for o primeiro peso:
            summatory += BIAS * (*(weights+i+index));           //multiplica o limiar (no caso o peso 0) pelo BIAS;
        } else {                                            //caso n�o:
            Var1=*(input+i-1);
            Var2=*(weights+i+index);
            summatory += Var1 * Var2;     //multiplica o peso pelas entradas;
        }
	}
	return summatory;
}

double neuronOutput(char axonFamily, double activation){

    double output;  //variavel que armazena a sa�da
    switch(axonFamily){
        case 'l':                           //funcao de ativacao: linear
            output = activation;
            break;
        case 't':                           //funcao de ativacao: tangente hiperbolica
            output = tanh(activation);
            break;
    }
    return output;
}

//void imprime(double num){
//	unsigned char buffer[60];
//	if (num < 0)	
//		sprintf(buffer,"-%d.%04u \t", (int) num, (int) fabs(((num - (int)num) * 10000)) );
//	else
//		sprintf(buffer," %d.%04u \t", (int) num, (int) fabs(((num - (int)num) * 10000)) );	
//    sendBufferUsart(buffer);	
//}	

int P=0;
unsigned char mlp(__psv__ double *w,__psv__ double *m,double *input){

	/* VARIAVEIS
        INT
        i, n = inteiros de controle;
        noOfNeurons = Armazena n�mero de Neuronios da camada atual;
        noOfWeights = Armazena n�mero de Pesos da camada atual;
        index = vari�vel de controle para indicar o indice correto do vetor de pesos;
        DOUBLE
        *weights = ponteiro que aponta para os pesos da camada atual;
        uh = vetor com os somat�rios (ativa��es) dos neuronios da camada escondida;
        yh = vetor com as saidas (ap�s fun��es de ativa��o) dos neuronios da camada escondida;
        uo = vetor com somat�rios (ativa��es) dos neuronios da camada de sa�da;
        yo = vetor com as sa�das (ap�s fun��es de ativa��o) dos neuronios da camada de saida;
    */

	int i,n,noOfNeurons,noOfWeights,index;
    double Max;
    unsigned char MaxIndex;
    
    __psv__ double *weights;
    double uh[NO_OF_INPUTS_HIDDEN_LAYER],yh[NO_OF_INPUTS_OUTPUT_LAYER],
		   uo[NO_OF_INPUTS_OUTPUT_LAYER],yo[NO_OF_OUTPUTS];

   
    //P=1;
	for (i=0;i<NO_OF_LAYERS;i++){ //percorre camada a camada da rede
		if (i==0){ //aponta a vari�vel para os pesos referentes a primeira camada (escondida);
			weights = w;
		}
		else if (i==1){ //aponta a vari�vel para os pesos referentes a segunda camada (saida);
			weights = m;
		}
		noOfNeurons = layers[i];         //recebe o n�mero de neuronios de cada camada;
		noOfWeights = noOfInputs[i] + 1; //recebe o n�mero de pesos de cada camada + 1 (limiar do neuronio);
        for (n=0;n<noOfNeurons;n++){     //percorre todos os neuronios da camada atual
            index = n * noOfWeights;     //recebe o valor referente ao indice que aponta o primeiro peso da camada no vetor de pesos correspondente
            if (i==0){
                uh[n] = neuronInput(weights,noOfWeights,index,input); //calcula somat�rio do neuronio e armazena no vetor;
                yh[n] = neuronOutput(axonFamilies[i],uh[n]);          //calcula ativa��o do neuronio e armazena no vetor;
            } else if (i==1){
                uo[n] = neuronInput(weights,noOfWeights,index,yh);    //calcula somat�rio do neuronio e armazena no vetor;
                yo[n] = neuronOutput(axonFamilies[i],uo[n]);          //calcula ativa��o do neuronio e armazena no vetor;
                //imprime(yo[n]);
                
            }
		}
	}
    P=1;
    
    Max=yo[0];
    MaxIndex=0;
    for(i=1;i< NO_OF_OUTPUTS; i++){
        if(yo[i]>Max){
            Max=yo[i];
            MaxIndex=i;
        }
    }
    
    return MaxIndex;
}


