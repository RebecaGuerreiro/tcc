/* 
 * File:   RNA.h
 * Author: Rebeca
 *
 * Created on 23 de Maio de 2016, 09:37
 */

#ifndef RNA_H
#define	RNA_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#define NO_OF_LAYERS 2 //n�mero de camadas;
#define NO_OF_OUTPUTS 3 //n�mero de sa�das;
#define NO_OF_INPUTS_HIDDEN_LAYER 6 //na primeira camada, sem bias;
#define NO_OF_INPUTS_OUTPUT_LAYER 7 //na primeira camada, sem bias;
#define BIAS -1    


//Fun��es da rede
double neuronInput(__psv__ double *weights,int noOfWeights,int index,double *input); // funcao que calcula o a ativacao(somatorio) do neuronio;
double neuronOutput(char axonFamily,double activation); //funcao que calcula a sa�da (funcao de ativacao) do neuronio;
unsigned char mlp(__psv__ double *w,__psv__ double *m,double *input); //funcao principal que executa o algoritmo;
void imprime(double num);

/*Declaration of Weights
w = pesos da entrada para camada escondida.
m = pesos da camada escondida para de saida;

Os pesos devem ser colocado no formato de vetor; os limiares (pesos referentes ao BIAS) de cada neuronio j� devem ser inclusos, sendo o primeiro de cada "linha";

Como exemplo:

    => Sendo uma matriz de pesos 5x7:
        - As linhas representam os neuronios (5 neuronios);
        - As colunas representam os pesos (1 limiar no primeiro indice da coluna + 6 pesos nos outros indices = 7);
    => A matriz deve ser adicionada em w como um vetor de 35 (5x7) onde:
        - 0 a 6, representam os pesos do primeiro neuronio, onde 0 � o limiar do neuronio e 1 a 6 s�o os pesos referentes as entradas
        - 7 a 13, representam os pesos do segundo neuronio, onde 7 � o limiar do neuronio e 8 a 13 s�o os pesos referentes as entradas
        - 14 a 20, representam os pesos do terceiro neuronio, onde 14 � o limiar do neuronio e 15 a 20 s�o os pesos referentes as entradas
        - 21 a 27, representam os pesos do quarto neuronio, onde 21 � o limiar do neuronio e 22 a 27 s�o os pesos referentes as entradas
        - 28 a 34, representam os pesos do quinto neuronio, onde 28 � o limiar do neuronio e 29 a 34 s�o os pesos referentes as entradas
*/


#ifdef	__cplusplus
}
#endif

#endif	/* RNA_H */

